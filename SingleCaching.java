import java.util.ArrayList;
import java.util.List;

public class SingleCaching {
	List<Integer> I0 = new ArrayList<>();
	List<Integer> r0 = new ArrayList<>();
	List<ArrayList<Integer>> ret0 = new ArrayList<>();
	int B=0;
	public int DP()
	{
		int t = I0.size();
		int step = 100;
		Integer[] I = I0.toArray(new Integer[t]);
		Integer[] r = r0.toArray(new Integer[t]);
		Integer[][] ret = ret0.toArray(new Integer[t][B/step]);
		for(int i=0;i<t;i++)
			for(int j=1;j*step<=B;j++)
			{
				if(j<I[i])
					ret[i][j]=ret[i][j-1];
				else
					ret[i][j]=Math.max(ret[i-1][j], r[i]+ret[i-1][j-I[i]]);
			}
		return ret[t][B/step];
	}
	public static void main(String[] args)
	{
		new SingleCaching().DP();
	}
}
